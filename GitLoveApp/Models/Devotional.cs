﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLoveApp.Models
{
    public class Devotional
    {
        [Key]
        public int DevotionalId { get; set; }
        [Required(ErrorMessage = "Your Post is required")]
        [DataType(DataType.MultilineText)]
        public string DevotionalPost { get; set; }
        public int Rating { get; set; }
        public DateTime PostDate { get; set; }

        [ForeignKey("CreatedByUser")]
        public virtual string CreatedById { get; set; }
        public ApplicationUser CreatedByUser { get; set; }

    }
}
