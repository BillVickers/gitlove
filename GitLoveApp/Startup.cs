﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitLoveApp.Startup))]
namespace GitLoveApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
