﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GitLoveApp.Models;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace GitLoveApp.Controllers
{
    public class DevotionalController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Devotional
        public ActionResult Index()
        {
            var devotionals = db.Devotionals.Include(d => d.CreatedByUser);
            return View(devotionals.ToList());
        }

        // GET: Devotional/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Devotional devotional = db.Devotionals.Find(id);
            if (devotional == null)
            {
                return HttpNotFound();
            }
            return View(devotional);
        }

        // GET: Devotional/Create
        [Authorize]
        public ActionResult Create()
        {
            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName");
            return View();
        }

        // POST: Devotional/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DevotionalPost")] Devotional devotional)
        {
            if (ModelState.IsValid)
            {
                devotional.Rating = 0;
                devotional.CreatedById = User.Identity.GetUserId();
                devotional.PostDate = DateTime.Now;

                db.Devotionals.Add(devotional);
                db.SaveChanges();

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "smtp.mail.yahoo.com";
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("bill-vickers@att.net", "GavinLucas1");
                mail.To.Add(new MailAddress("bill-vickers@att.net"));
                mail.From = new MailAddress("househunter.realty@att.net");
                mail.Subject = "Something was posted";
                mail.Body = "You have received a post in GitLove.org.";
                client.Send(mail);

                return RedirectToAction("ThankYou");
            }

            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName", devotional.CreatedById);
            return View(devotional);
        }

        // GET: Devotional/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Devotional devotional = db.Devotionals.Find(id);
            if (devotional == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName", devotional.CreatedById);
            return View(devotional);
        }

        // POST: Devotional/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DevotionalId,DevotionalPost,Rating,PostDate,CreatedById")] Devotional devotional)
        {
            if (ModelState.IsValid)
            {
                db.Entry(devotional).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName", devotional.CreatedById);
            return View(devotional);
        }

        // GET: Devotional/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Devotional devotional = db.Devotionals.Find(id);
            if (devotional == null)
            {
                return HttpNotFound();
            }
            return View(devotional);
        }

        // POST: Devotional/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Devotional devotional = db.Devotionals.Find(id);
            db.Devotionals.Remove(devotional);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult ThankYou()
        {
            
            return View();
        }
    }
}
